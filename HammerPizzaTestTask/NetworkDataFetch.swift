//
//  NetworkDataFetch.swift
//  HammerPizzaTestTask
//
//  Created by Sasha Styazhkin on 29.10.2021.
//

import Foundation

class NetworkDataFetch {
    
    static let shared = NetworkDataFetch()
    
    private init() {}
    
    func fetchMenuItem (urlString: String, responce: @escaping ([MenuItem]?, Error?) -> Void) {
        
        NetworkRequest.shared.requestData(urlString: urlString) { result in
            switch result {
            
            case .success(let data):
                do {
                    let items = try JSONDecoder().decode([MenuItem].self, from: data)
                    responce(items, nil)
                } catch let jsonError {
                    print("JSON decode failure.", jsonError.localizedDescription)
                }
                
            case .failure(let error):
                print("Error with data.", error.localizedDescription)
                responce(nil, error)
                
            }
        }
        
    }
    
}
