//
//  MenuItemCollectionViewCell.swift
//  HammerPizzaTestTask
//
//  Created by Sasha Styazhkin on 28.10.2021.
//

import UIKit

class MenuItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuItemImageView: UIImageView!
    
    @IBOutlet weak var menuItemNameLabel: UILabel!
    
    @IBOutlet weak var menuItemContentsLabel: UILabel!
    
}
