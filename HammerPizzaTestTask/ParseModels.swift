//
//  ParseModels.swift
//  HammerPizzaTestTask
//
//  Created by Sasha Styazhkin on 29.10.2021.
//

import Foundation

//struct MenuModel: Decodable {
//    let results: [MenuItem]
//}

struct MenuItem: Decodable {
    let category: String
    let name: String
    let topping: [String]?
}
