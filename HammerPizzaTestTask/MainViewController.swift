//
//  MainViewController.swift
//  HammerPizzaTestTask
//
//  Created by Sasha Styazhkin on 28.10.2021.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    
    @IBOutlet weak var topShadowView: UIView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var menuHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainScrollView: UIScrollView!

    @IBOutlet weak var tabCollectionView: UICollectionView!
    
    var menuItems = [MenuItem]()
    var categories = ["Pizza", "Dryck", "Tillbehör", "Pizza", "Dryck", "Tillbehör"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        menuCollectionView.delegate = self
        menuCollectionView.dataSource = self
        
        topShadowView.layer.masksToBounds = false
        
        topShadowView.layer.shadowColor = UIColor(red: 0.646, green: 0.646, blue: 0.646, alpha: 0.24).cgColor
        topShadowView.layer.shadowRadius = 14
        topShadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        topShadowView.layer.shadowOpacity = 1
        let shadowSize: CGFloat = 30
        let shadowPath = CGPath(ellipseIn: CGRect(x: -shadowSize, y: topShadowView.bounds.height - shadowSize * 0.5, width: topShadowView.bounds.width + shadowSize * 2, height: shadowSize), transform: nil)
        topShadowView.layer.shadowPath = shadowPath
        
        tabView.layer.masksToBounds = false
        tabView.layer.shadowColor = UIColor(red: 0.646, green: 0.646, blue: 0.646, alpha: 0.24).cgColor
        tabView.layer.shadowRadius = 10
        tabView.layer.shadowOffset = CGSize(width: 0, height: 0);
        tabView.layer.shadowOpacity = 1
        
        menuCollectionView.layer.cornerRadius = 30
        
        
        fetchMenuItems(restarauntId: 1)
        
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case bannerCollectionView:
            return 2
        case categoryCollectionView:
            return categories.count
        case menuCollectionView:
            return menuItems.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bannerCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as! BannerCollectionViewCell
            cell.bannerImageView.image = UIImage(named: "banner")
            return cell
            
        } else if collectionView == categoryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
            cell.categoryNameLabel.text = categories[indexPath.row]
            return cell
        } else if collectionView == menuCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuItemCell", for: indexPath) as! MenuItemCollectionViewCell
            let item = menuItems[indexPath.row]
            cell.menuItemNameLabel.text = item.name
            cell.menuItemContentsLabel.text = item.topping?.joined(separator: ", ")
            cell.menuItemImageView.image = UIImage(named: item.category)
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    

    
    
    
    func fetchMenuItems(restarauntId: Int) {
        let urlString = "https://private-anon-703cd9e6d5-pizzaapp.apiary-mock.com/restaurants/\(restarauntId)/menu"
        
        NetworkDataFetch.shared.fetchMenuItem(urlString: urlString) { [weak self] menuModel, error in
            
            if error != nil {
                print(error?.localizedDescription)
            } else {
                guard let menuModel = menuModel else { return }
                
                self?.menuItems = menuModel
                self?.menuCollectionView.reloadData()
                self?.menuHeightConstraint.constant = (self?.menuCollectionView.collectionViewLayout.collectionViewContentSize.height)!
                self?.menuCollectionView.setNeedsLayout()
                print(self?.menuItems)
            }
        }
    }
    
//    override func viewWillLayoutSubviews() {
//        super.updateViewConstraints()
//        self.menuHeightConstraint?.constant = self.menuCollectionView.contentSize.height
//
//
//    }


}
